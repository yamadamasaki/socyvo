import type { Principal } from '@dfinity/principal';
import type { ActorMethod } from '@dfinity/agent';

export interface Personoid {}
export interface _SERVICE extends Personoid {}
