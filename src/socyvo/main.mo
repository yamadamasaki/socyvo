import Cycles "mo:base/ExperimentalCycles";
import HashMap "mo:base/HashMap";
import Iter "mo:base/Iter";
import Text "mo:base/Text";
import D "mo:base/Debug";
import Personoids "../Personoids/main";
import PersonoidTypes "../Personoids/types";
import Organoids "../Organoids/main";
import OrganoidTypes "../Organoids/types";
import Groupoids "../Groupoids/main";
import GroupoidTypes "../Groupoids/types"

actor Socyvo {
  type Personoid = {
    getProperties: shared () -> async PersonoidTypes.Properties
  };
  let personoids: HashMap.HashMap<Text, Personoids.Personoid> = HashMap.HashMap(0, Text.equal, Text.hash);

  public func createPersonoid(name: Text) : async () {
    if (Iter.size((Iter.filter(personoids.keys(), func (it: Text) : Bool { Text.equal(it, name) }))) == 0) {
      Cycles.add(1_000_000_000_000);
      let personoid = await Personoids.Personoid(name);
      personoids.put(name, personoid)
    }
  };

  public func getPersonoids() : async [Text] { Iter.toArray(personoids.keys()) };

  public func deletePersonoid(name: Text) : async () { personoids.delete(name) };

  type Organoid = {
    getProperties: shared () -> async OrganoidTypes.Properties
  };
  let organoids: HashMap.HashMap<Text, Organoids.Organoid> = HashMap.HashMap(0, Text.equal, Text.hash);

  public func createOrganoid(name: Text) : async () {
    if (Iter.size((Iter.filter(organoids.keys(), func (it: Text) : Bool { Text.equal(it, name) }))) == 0) {
      Cycles.add(1_000_000_000_000);
      let organoid = await Organoids.Organoid(name);
      organoids.put(name, organoid)
    }
  };

  public func getOrganoids() : async [Text] { Iter.toArray(organoids.keys()) };

  public func deleteOrganoid(name: Text) : async () { organoids.delete(name) };

  type Groupoid = {
    getProperties: shared () -> async GroupoidTypes.Properties
  };
  let groupoids: HashMap.HashMap<Text, Groupoids.Groupoid> = HashMap.HashMap(0, Text.equal, Text.hash);

  public func createGroupoid(name: Text) : async () {
    if (Iter.size((Iter.filter(groupoids.keys(), func (it: Text) : Bool { Text.equal(it, name) }))) == 0) {
      Cycles.add(1_000_000_000_000);
      let groupoid = await Groupoids.Groupoid(name);
      groupoids.put(name, groupoid)
    }
  };

  public func getGroupoids() : async [Text] { Iter.toArray(groupoids.keys()) };

  public func deleteGroupoid(name: Text) : async () { groupoids.delete(name) }

}
