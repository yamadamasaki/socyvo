module {
  public type AdvertisementId = Text;

  public type Advertisement = {
    kind: Text;
    description: Text;
    descriptionFormat: Text;
    url: Text;
    tags: [Text];
    advertiseFromTime: Int;
    advertiseToTime: Int;
    apply: shared () -> ();
    inquiry: shared ?() -> ();
    filter: shared ?() -> ()
  }
}
