import D = "mo:base/Debug";
import Types = "./types";
import Advertisement = "../advertisement/advertisement"

shared(install) actor class Passage(name: Text) {
  let creator = install.caller;
  var properties = {name; creator};

  type AdvertisementId = Advertisement.AdvertisementId;
  type Advertisement = Advertisement.Advertisement;

  public func getProperties() : async Types.Properties {
    D.print(debug_show(properties));
    properties
  };

  public func advertise(advertisement: Advertisement) : async AdvertisementId {
    ""
  };

  public func unadvertise(advertisementId: AdvertisementId) : async () {

  }
}
