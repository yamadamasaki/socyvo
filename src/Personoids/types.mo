module {
  public type Properties = {
    name: Text;
    creator: Principal
  }
}
