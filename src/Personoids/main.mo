import D "mo:base/Debug";
import Types = "./types"

shared(install) actor class Personoid(name: Text) {
  let creator = install.caller;
  var properties = {name; creator};

  public func getProperties() : async Types.Properties {
    D.print(debug_show(properties));
    properties
  }
}
